const size = 250;

function setup() {
  createCanvas(size * 3, size * 2);
}

function draw() {
  const dir = (180 - ((sin(2 / sqrt(13)) * PI / 180) ** -1)) / 2;
  
  // 중앙 태극 문양
  noStroke();
  translate(size * 1.5, size);
  
  push();
  rotate(dir * PI / 180);
  fill(255, 0, 0);
  arc(0, 0, size, size, PI, 0, PIE);
  fill(0, 0, 255);
  arc(0, 0, size, size, 0, PI, PIE);
  pop();
  
  push();
  rotate(dir * PI / 180);
  translate(-(size / 4), 0);
  fill(255, 0, 0);
  circle(0, 0, size / 2);
  translate(size / 2, 0);
  fill(0, 0, 255);
  circle(0, 0, size / 2);
  pop();
  
  // 건곤감리
  // 건
  push();
  rotate(dir * PI / 180);
  
  translate(- 0.75 * size - size / 12, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  
  translate(-size / 12 - size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  
  translate(-size / 12 - size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  
  pop();
  
  // 곤
  push();
  rotate(dir * PI / 180);
  
  translate(0.75 * size, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  fill(255)
  rect(-1, - size / 24 / 2, size / 12 + 2, size / 24);
  
  translate(size / 12 + size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  fill(255)
  rect(-1, - size / 24 / 2, size / 12 + 2, size / 24);
  
  translate(size / 12 + size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  fill(255)
  rect(-1, - size / 24 / 2, size / 12 + 2, size / 24);
  
  pop();
  
  // 이
  push();
  rotate(- dir * PI / 180);
  
  translate(- 0.75 * size - size / 12, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  
  translate(-size / 12 - size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  fill(255)
  rect(-1, - size / 24 / 2, size / 12 + 2, size / 24);
  
  translate(-size / 12 - size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  
  pop();
  
  // 감
  push();
  rotate(- dir * PI / 180);
  
  translate(0.75 * size, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  fill(255)
  rect(-1, - size / 24 / 2, size / 12 + 2, size / 24);
  
  translate(size / 12 + size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  
  translate(size / 12 + size / 24, 0);
  fill(0);
  rect(0, - size / 4, size / 12, size / 2);
  fill(255)
  rect(-1, - size / 24 / 2, size / 12 + 2, size / 24);
  
  pop();
}